# Lithuanian translations for trunk-kf package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the trunk-kf package.
#
# Automatically generated, 2014.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: trunk-kf 5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2022-11-18 00:54+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.1\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "Atverti vidinį aplanką"

#: baloosearchrunner.cpp:82
#, fuzzy, kde-format
#| msgid "Audio"
msgid "Audios"
msgstr "Garso įrašas"

#: baloosearchrunner.cpp:83
#, fuzzy, kde-format
#| msgid "Image"
msgid "Images"
msgstr "Paveikslas"

#: baloosearchrunner.cpp:84
#, fuzzy, kde-format
#| msgid "Video"
msgid "Videos"
msgstr "Vaizdo įrašas"

#: baloosearchrunner.cpp:85
#, fuzzy, kde-format
#| msgid "Spreadsheet"
msgid "Spreadsheets"
msgstr "Skaičiuoklė"

#: baloosearchrunner.cpp:86
#, fuzzy, kde-format
#| msgid "Presentation"
msgid "Presentations"
msgstr "Pateiktis"

#: baloosearchrunner.cpp:87
#, fuzzy, kde-format
#| msgid "Folder"
msgid "Folders"
msgstr "Aplankas"

#: baloosearchrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Document"
msgid "Documents"
msgstr "Dokumentas"

#: baloosearchrunner.cpp:89
#, fuzzy, kde-format
#| msgid "Archive"
msgid "Archives"
msgstr "Archyvas"

#: baloosearchrunner.cpp:90
#, fuzzy, kde-format
#| msgid "Text"
msgid "Texts"
msgstr "Tekstas"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr ""

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Ieškoti failuose, el. laiškuose ir kontaktuose"
