# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# giovanni <g.sora@tiscali.it>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-19 01:53+0000\n"
"PO-Revision-Date: 2023-11-19 17:45+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/BrightnessItem.qml:70
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Brightness and Color"
msgstr "Brillantia e Color"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Brillantia de schermo a %1 %"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Brillantia de claviero a %1 %"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "Color de Nocte non active"

#: package/contents/ui/main.qml:107
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "Color de Nocte a %1K"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Rola pro adjustar luminositate de schermo"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "Cliccar in le medie commuta Color de Nocte"

#: package/contents/ui/NightColorItem.qml:74
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "De-Activate (Off)"

#: package/contents/ui/NightColorItem.qml:77
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Non disponibile"

#: package/contents/ui/NightColorItem.qml:80
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Non habilitate"

#: package/contents/ui/NightColorItem.qml:83
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Il non es in execution"

#: package/contents/ui/NightColorItem.qml:86
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Activate (On)"

#: package/contents/ui/NightColorItem.qml:89
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Transition de Matino"

#: package/contents/ui/NightColorItem.qml:91
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "Die"

#: package/contents/ui/NightColorItem.qml:93
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Transition de vespere"

#: package/contents/ui/NightColorItem.qml:95
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Nocte"

#: package/contents/ui/NightColorItem.qml:105
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:139
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#: package/contents/ui/NightColorItem.qml:139
#, kde-format
msgid "Enable and Configure…"
msgstr "Habilita e Configuira..."

#: package/contents/ui/NightColorItem.qml:165
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Transition a die complete per:"

#: package/contents/ui/NightColorItem.qml:167
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Transition a nocte previste per:"

#: package/contents/ui/NightColorItem.qml:169
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Transition a nocte complete per:"

#: package/contents/ui/NightColorItem.qml:171
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Tramsition a die previste per:"

#: package/contents/ui/PopupDialog.qml:70
#, kde-format
msgid "Display Brightness"
msgstr "Brillantia de Monstrator"

#: package/contents/ui/PopupDialog.qml:101
#, kde-format
msgid "Keyboard Brightness"
msgstr "Intensitate de illumination de claviero"

#: package/contents/ui/PopupDialog.qml:132
#, kde-format
msgid "Night Light"
msgstr "Color de Nocte"
