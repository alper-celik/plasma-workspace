# Translation of plasma_wallpaper_image.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2008, 2009, 2014.
# Manfred Wiese <m.j.wiese@web.de>, 2009, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2014-08-11 12:41+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, fuzzy, kde-format
#| msgid "Open Image"
msgctxt "@title:window"
msgid "Open Image"
msgstr "Bild opmaken"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, fuzzy, kde-format
#| msgid "Directory with the wallpaper to show slides from"
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Orner mit de Achtergrundbiller för der Diaschau"

#: imagepackage/contents/ui/config.qml:100
#, kde-format
msgid "Positioning:"
msgstr "Steed:"

#: imagepackage/contents/ui/config.qml:103
#, fuzzy, kde-format
#| msgid "Scaled && Cropped"
msgid "Scaled and Cropped"
msgstr "Topasst un sneden"

#: imagepackage/contents/ui/config.qml:107
#, kde-format
msgid "Scaled"
msgstr "Topasst"

#: imagepackage/contents/ui/config.qml:111
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Topasst, Proportschoon wohren"

#: imagepackage/contents/ui/config.qml:115
#, kde-format
msgid "Centered"
msgstr "In de Merrn"

#: imagepackage/contents/ui/config.qml:119
#, kde-format
msgid "Tiled"
msgstr "Kachelt"

#: imagepackage/contents/ui/config.qml:147
#, fuzzy, kde-format
#| msgid "Background Color:"
msgid "Background:"
msgstr "Achtergrundklöör:"

#: imagepackage/contents/ui/config.qml:148
#, kde-format
msgid "Blur"
msgstr ""

#: imagepackage/contents/ui/config.qml:157
#, kde-format
msgid "Solid color"
msgstr ""

#: imagepackage/contents/ui/config.qml:167
#, fuzzy, kde-format
#| msgid "Background Color:"
msgid "Select Background Color"
msgstr "Achtergrundklöör:"

#: imagepackage/contents/ui/main.qml:34
#, fuzzy, kde-format
#| msgid "Next Wallpaper Image"
msgid "Open Wallpaper Image"
msgstr "Nakamen Achtergrundbild"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Nakamen Achtergrundbild"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, fuzzy, kde-format
#| msgid "Images"
msgid "Images"
msgstr "Biller"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr ""

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, fuzzy, kde-format
#| msgid "Download Wallpapers"
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Achtergrundbiller daalladen"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Restore wallpaper"
msgstr "Achtergrundbild wegmaken"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Remove Wallpaper"
msgstr "Achtergrundbild wegmaken"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, fuzzy, kde-format
#| msgid "Images"
msgid "Image Files"
msgstr "Biller"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:37
#, kde-format
msgid "Order:"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:44
#, kde-format
msgid "Random"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "A to Z"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Z to A"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "Date modified (newest first)"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Date modified (oldest first)"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:85
#, kde-format
msgid "Group by folders"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:97
#, kde-format
msgid "Change every:"
msgstr "Biller ännern elk:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:107
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:127
#, fuzzy, kde-format
#| msgid "Minutes"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "Minuten"
msgstr[1] "Minuten"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:147
#, fuzzy, kde-format
#| msgid "Seconds"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "Sekunnen"
msgstr[1] "Sekunnen"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:178
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Folders"
msgstr "Achtergrundbild wegmaken"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:182
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:220
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Remove Folder"
msgstr "Achtergrundbild wegmaken"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:231
#, fuzzy, kde-format
#| msgid "Add Folder"
msgid "Open Folder…"
msgstr "Orner tofögen"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:246
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, fuzzy, kde-format
#| msgid "Download Wallpapers"
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Achtergrundbiller daalladen"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""

#, fuzzy
#~| msgid "Add Folder"
#~ msgid "Add Folder…"
#~ msgstr "Orner tofögen"

#~ msgid "Recommended wallpaper file"
#~ msgstr "Anraadt Achtergrundbild-Datei"

#, fuzzy
#~| msgid "Remove wallpaper"
#~ msgid "Add Custom Wallpaper"
#~ msgstr "Achtergrundbild wegmaken"

#~ msgid "Remove wallpaper"
#~ msgstr "Achtergrundbild wegmaken"

#~ msgid "%1 by %2"
#~ msgstr "%1 vun %2"

#, fuzzy
#~| msgid "Remove wallpaper"
#~ msgid "Wallpapers"
#~ msgstr "Achtergrundbild wegmaken"

#~ msgid "Download Wallpapers"
#~ msgstr "Achtergrundbiller daalladen"

#~ msgid "Hours"
#~ msgstr "Stünnen"

#~ msgid "Open..."
#~ msgstr "Opmaken..."

#~ msgctxt "Unknown Author"
#~ msgid "Unknown"
#~ msgstr "Nich begäng"

#~ msgid "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"
#~ msgstr "Bilddateien (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"

#~ msgid "Screenshot"
#~ msgstr "Schirmfoto"

#~ msgid "Preview"
#~ msgstr "Vöransicht"
