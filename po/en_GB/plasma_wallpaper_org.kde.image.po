# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2023-02-27 21:39+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, fuzzy, kde-format
#| msgid "Open Image"
msgctxt "@title:window"
msgid "Open Image"
msgstr "Open Image"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, fuzzy, kde-format
#| msgid "Directory with the wallpaper to show slides from"
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Directory with the wallpaper to show slides from"

#: imagepackage/contents/ui/config.qml:100
#, kde-format
msgid "Positioning:"
msgstr "Positioning:"

#: imagepackage/contents/ui/config.qml:103
#, kde-format
msgid "Scaled and Cropped"
msgstr "Scaled and Cropped"

#: imagepackage/contents/ui/config.qml:107
#, kde-format
msgid "Scaled"
msgstr "Scaled"

#: imagepackage/contents/ui/config.qml:111
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Scaled, Keep Proportions"

#: imagepackage/contents/ui/config.qml:115
#, kde-format
msgid "Centered"
msgstr "Centred"

#: imagepackage/contents/ui/config.qml:119
#, kde-format
msgid "Tiled"
msgstr "Tiled"

#: imagepackage/contents/ui/config.qml:147
#, kde-format
msgid "Background:"
msgstr "Background:"

#: imagepackage/contents/ui/config.qml:148
#, kde-format
msgid "Blur"
msgstr "Blur"

#: imagepackage/contents/ui/config.qml:157
#, kde-format
msgid "Solid color"
msgstr "Solid colour"

#: imagepackage/contents/ui/config.qml:167
#, kde-format
msgid "Select Background Color"
msgstr "Select Background Colour"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Open Wallpaper Image"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Next Wallpaper Image"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, fuzzy, kde-format
#| msgid "Image Files"
msgid "Images"
msgstr "Image Files"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr ""

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, fuzzy, kde-format
#| msgid "Get New Wallpapers…"
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Get New Wallpapers…"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Open Containing Folder"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Restore wallpaper"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Remove Wallpaper"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""
"This tool allows you to set an image as the wallpaper for the Plasma session."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "An error occurred while attempting to set the Plasma wallpaper:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr "Successfully set the wallpaper for all desktops to the image %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Image Files"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:37
#, kde-format
msgid "Order:"
msgstr "Order:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:44
#, kde-format
msgid "Random"
msgstr "Random"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "A to Z"
msgstr "A to Z"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Z to A"
msgstr "Z to A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "Date modified (newest first)"
msgstr "Date modified (newest first)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Date modified (oldest first)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:85
#, kde-format
msgid "Group by folders"
msgstr "Group by folders"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:97
#, kde-format
msgid "Change every:"
msgstr "Change every:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:107
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hour"
msgstr[1] "%1 hours"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:127
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minute"
msgstr[1] "%1 minutes"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:147
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 second"
msgstr[1] "%1 seconds"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:178
#, kde-format
msgid "Folders"
msgstr "Folders"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:182
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:220
#, kde-format
msgid "Remove Folder"
msgstr "Remove Folder"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:231
#, fuzzy, kde-format
#| msgid "Open Folder"
msgid "Open Folder…"
msgstr "Open Folder"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:246
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "There are no wallpaper locations configured"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Set as Wallpaper"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr "Desktop"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr "Lockscreen"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr "Both"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""
"An error occurred while attempting to open kscreenlockerrc configuration "
"file."

#~ msgid "Add Image…"
#~ msgstr "Add Image…"

#~ msgid "Add Folder…"
#~ msgstr "Add Folder…"

#~ msgid "Recommended wallpaper file"
#~ msgstr "Recommended wallpaper file"

#~ msgid "There are no wallpapers in this slideshow"
#~ msgstr "There are no wallpapers in this slideshow"

#~ msgid "Add Custom Wallpaper"
#~ msgstr "Add Custom Wallpaper"

#~ msgid "Remove wallpaper"
#~ msgstr "Remove wallpaper"

#~ msgid "%1 by %2"
#~ msgstr "%1 by %2"

#, fuzzy
#~| msgid "Remove Wallpaper"
#~ msgid "Wallpapers"
#~ msgstr "Remove Wallpaper"

#~ msgctxt "<image> by <author>"
#~ msgid "By %1"
#~ msgstr "By %1"

#~ msgid "Download Wallpapers"
#~ msgstr "Download Wallpapers"

#~ msgid "Hours"
#~ msgstr "Hours"

#~ msgid "Open..."
#~ msgstr "Open..."

#~ msgctxt "Unknown Author"
#~ msgid "Unknown"
#~ msgstr "Unknown"

#~ msgid "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"
#~ msgstr "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"

#~ msgid "Screenshot"
#~ msgstr "Screenshot"

#~ msgid "Preview"
#~ msgstr "Preview"
