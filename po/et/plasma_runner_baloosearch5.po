# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2019-10-30 21:01+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.1\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "Ava vastav kataloog"

#: baloosearchrunner.cpp:82
#, fuzzy, kde-format
#| msgid "Audio"
msgid "Audios"
msgstr "Heli"

#: baloosearchrunner.cpp:83
#, fuzzy, kde-format
#| msgid "Image"
msgid "Images"
msgstr "Pilt"

#: baloosearchrunner.cpp:84
#, fuzzy, kde-format
#| msgid "Video"
msgid "Videos"
msgstr "Video"

#: baloosearchrunner.cpp:85
#, fuzzy, kde-format
#| msgid "Spreadsheet"
msgid "Spreadsheets"
msgstr "Arvutustabel"

#: baloosearchrunner.cpp:86
#, fuzzy, kde-format
#| msgid "Presentation"
msgid "Presentations"
msgstr "Esitlus"

#: baloosearchrunner.cpp:87
#, fuzzy, kde-format
#| msgid "Folder"
msgid "Folders"
msgstr "Kataloog"

#: baloosearchrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Document"
msgid "Documents"
msgstr "Dokument"

#: baloosearchrunner.cpp:89
#, fuzzy, kde-format
#| msgid "Archive"
msgid "Archives"
msgstr "Arhiiv"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr ""

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr ""

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Otsimine failides, e-kirjades ja kontaktides"
