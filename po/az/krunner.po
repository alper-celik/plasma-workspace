# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-28 01:37+0000\n"
"PO-Revision-Date: 2022-11-11 11:58+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xəyyam Qocayev"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xxmn77@gmail.com"

#: main.cpp:73 view.cpp:49
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:73
#, kde-format
msgid "Run Command interface"
msgstr "Əmr interfeysini işə salmaq"

#: main.cpp:79
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Mübadilə buferi tərkiblərini KRunner üçün sorğu kimi istifadə etmək"

#: main.cpp:80
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "KRunneri arxa planda başlatmaq, onu göstərməmək."

#: main.cpp:81
#, kde-format
msgid "Replace an existing instance"
msgstr "Mövcud nümunə ilə əvəzləmək"

#: main.cpp:82
#, kde-format
msgid "Show only results from the given plugin"
msgstr ""

#: main.cpp:83
#, kde-format
msgid "List available plugins"
msgstr ""

#: main.cpp:90
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "Başlatmaq üçün sorğu, əgər -c açarı göstərilməyibsə istifadə etmək"

#: main.cpp:99
#, kde-format
msgctxt "Header for command line output"
msgid "Available KRunner plugins, pluginId"
msgstr ""

#: qml/RunCommand.qml:100
#, kde-format
msgid "Configure"
msgstr "Tənzimlə"

#: qml/RunCommand.qml:101
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "KRunner davranışlarını tənzimlə"

#: qml/RunCommand.qml:104
#, kde-format
msgid "Configure KRunner…"
msgstr "KRunner-i ayarla…"

#: qml/RunCommand.qml:117
#, kde-format
msgid "Showing only results from %1"
msgstr ""

#: qml/RunCommand.qml:131
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "\"%1\" axtarışı..."

#: qml/RunCommand.qml:132
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "Axtarış…"

#: qml/RunCommand.qml:303 qml/RunCommand.qml:304 qml/RunCommand.qml:306
#, kde-format
msgid "Show Usage Help"
msgstr "İstifadə olunma qaydalarını göstərmək"

#: qml/RunCommand.qml:314
#, kde-format
msgid "Pin"
msgstr "Sancaqla"

#: qml/RunCommand.qml:315
#, kde-format
msgid "Pin Search"
msgstr "Axtarışı sancaqla"

#: qml/RunCommand.qml:317
#, kde-format
msgid "Keep Open"
msgstr "Açıq saxla"

#: qml/RunCommand.qml:395 qml/RunCommand.qml:400
#, kde-format
msgid "Recent Queries"
msgstr "Sonuncu sorğular"

#: qml/RunCommand.qml:398
#, kde-format
msgid "Remove"
msgstr "Sils"

#~ msgid "krunner"
#~ msgstr "krunner"
