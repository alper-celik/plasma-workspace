# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2015, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-02 01:43+0000\n"
"PO-Revision-Date: 2022-06-11 09:40-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ਅ ਸ ਆਲਮ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alam.yellow@gmail.com"

#: kcm.cpp:465
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr "ਇਹਨਾਂ ਤਬਦੀਲੀਆਂ ਨੂੰ ਲਾਗੂ ਕਰਨ ਲਈ ਤੁਹਾਨੂੰ ਪਲਾਜ਼ਮਾ ਸ਼ੈਸ਼ਨ ਮੁੜ ਚਾਲੂ ਕਰਨਾ ਪਵੇਗਾ।"

#: kcm.cpp:466
#, kde-format
msgid "Cursor Settings Changed"
msgstr "ਕਰਸਰ ਸੈਟਿੰਗਾਂ ਬਦਲੀਆਂ ਗਈਆਂ"

#: lnftool.cpp:33
#, kde-format
msgid "Global Theme Tool"
msgstr "ਗਲੋਬਲ ਥੀਮ ਟੂਲ"

#: lnftool.cpp:35
#, kde-format
msgid ""
"Command line tool to apply global theme packages for changing the look and "
"feel."
msgstr ""

#: lnftool.cpp:37
#, kde-format
msgid "Copyright 2017, Marco Martin"
msgstr "Copyright 2017, ਮਾਈਕਰੋ ਮਾਰਟੇਨ"

#: lnftool.cpp:38
#, kde-format
msgid "Marco Martin"
msgstr "ਮਾਈਕਰੋ ਮਾਰਟੇਨ"

#: lnftool.cpp:38
#, kde-format
msgid "Maintainer"
msgstr "ਪਰਬੰਧਕ"

#: lnftool.cpp:46
#, kde-format
msgid "List available global theme packages"
msgstr ""

#: lnftool.cpp:49
#, kde-format
msgid ""
"Apply a global theme package. This can be the name of a package, or a full "
"path to an installed package, at which point this tool will ensure it is a "
"global theme package and then attempt to apply it"
msgstr ""

#: lnftool.cpp:51
#, kde-format
msgid "packagename"
msgstr "ਪੈਕੇਜ ਦਾ ਨਾਂ"

#: lnftool.cpp:52
#, kde-format
msgid "Reset the Plasma Desktop layout"
msgstr "ਪਲਾਜ਼ਮਾ ਡੈਸਕਟਾਪ ਖਾਕਾ ਮੁੜ-ਸੈੱਟ ਕਰੋ"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: lookandfeelsettings.kcfg:9
#, kde-format
msgid "Global look and feel"
msgstr "ਗਲੋਬਲ ਦਿੱਖ ਤੇ ਛੋਹ"

#: ui/main.qml:62
#, kde-format
msgctxt "@title:window"
msgid "Delete Permanently"
msgstr ""

#: ui/main.qml:63
#, kde-format
msgctxt "@label"
msgid "Do you really want to permanently delete this theme?"
msgstr ""

#: ui/main.qml:67
#, kde-format
msgctxt "@action:button"
msgid "Delete Permanently"
msgstr ""

#: ui/main.qml:72
#, fuzzy, kde-format
#| msgid "Cancel"
msgctxt "@action:button"
msgid "Cancel"
msgstr "ਰੱਦ ਕਰੋ"

#: ui/main.qml:88
#, kde-format
msgid "Get New…"
msgstr ""

#: ui/main.qml:103
#, kde-format
msgid "Contains Desktop layout"
msgstr "ਡੈਸਕਟਾਪ ਖਾਕਾ ਰੱਖਦਾ ਹੈ"

#: ui/main.qml:117
#, kde-format
msgid "Preview Theme"
msgstr "ਥੀਮ ਦੀ ਝਲਕ"

#: ui/main.qml:125
#, fuzzy, kde-format
#| msgid "Preview Theme"
msgctxt "@action:button"
msgid "Remove Theme"
msgstr "ਥੀਮ ਦੀ ਝਲਕ"

#: ui/main.qml:141
#, kde-format
msgctxt ""
"Confirmation question about applying the Global Theme - %1 is the Global "
"Theme's name"
msgid "Apply %1?"
msgstr "%1 ਲਾਗੂ ਕਰਨਾ ਹੈ?"

#: ui/main.qml:159
#, kde-format
msgid "Choose what to apply…"
msgstr "ਚੁਣੋ ਕਿ ਕੀ ਲਾਗੂ ਕਰਨਾ ਹੈ…"

#: ui/main.qml:159
#, kde-format
msgid "Show fewer options…"
msgstr "ਘੱਟ ਚੋਣਾਂ ਵੇਖਾਓ…"

#: ui/main.qml:176
#, kde-format
msgid "Apply"
msgstr "ਲਾਗੂ ਕਰੋ"

#: ui/main.qml:188
#, kde-format
msgid "Cancel"
msgstr "ਰੱਦ ਕਰੋ"

#: ui/MoreOptions.qml:23
#, kde-format
msgid "Layout settings:"
msgstr "ਖਾਕਾ ਸੈਟਿੰਗਾਂ:"

#: ui/MoreOptions.qml:27
#, kde-format
msgid "Desktop layout"
msgstr "ਡੈਸਕਟਾਪ ਖਾਕਾ"

#: ui/MoreOptions.qml:32
#, kde-format
msgid "Titlebar Buttons layout"
msgstr ""

#: ui/MoreOptions.qml:46 ui/SimpleOptions.qml:59
#, kde-format
msgid ""
"Applying a Desktop layout replaces your current configuration of desktops, "
"panels, docks, and widgets"
msgstr ""

#: ui/MoreOptions.qml:54
#, kde-format
msgid "Appearance settings:"
msgstr "ਦਿੱਖ ਸੈਟਿੰਗਾਂ:"

#: ui/MoreOptions.qml:58
#, kde-format
msgid "Colors"
msgstr "ਰੰਗ"

#: ui/MoreOptions.qml:59
#, kde-format
msgid "Application Style"
msgstr "ਐਪਲੀਕੇਸ਼ਨ ਸਟਾਈਲ"

#: ui/MoreOptions.qml:60
#, fuzzy, kde-format
#| msgid "Window Decorations"
msgid "Window Decoration Style"
msgstr "ਵਿੰਡੋ ਸਜਾਵਟ"

#: ui/MoreOptions.qml:61
#, fuzzy, kde-format
#| msgid "Window Decorations"
msgid "Window Decoration Size"
msgstr "ਵਿੰਡੋ ਸਜਾਵਟ"

#: ui/MoreOptions.qml:62
#, kde-format
msgid "Icons"
msgstr "ਆਈਕਾਨ"

#: ui/MoreOptions.qml:63
#, kde-format
msgid "Plasma Style"
msgstr "ਪਲਾਜ਼ਮਾ ਸਟਾਈਲ"

#: ui/MoreOptions.qml:64
#, kde-format
msgid "Cursors"
msgstr "ਕਰਸਰ"

#: ui/MoreOptions.qml:65
#, kde-format
msgid "Fonts"
msgstr "ਫ਼ੋਂਟ"

#: ui/MoreOptions.qml:66
#, kde-format
msgid "Task Switcher"
msgstr "ਟਾਸਕ ਸਵਿੱਚਰ"

#: ui/MoreOptions.qml:67
#, kde-format
msgid "Splash Screen"
msgstr "ਸਵਾਗਤੀ ਸਕਰੀਨ"

#: ui/MoreOptions.qml:68
#, kde-format
msgid "Lock Screen"
msgstr "ਲਾਕ ਸਕਰੀਨ"

#: ui/SimpleOptions.qml:19
#, kde-format
msgid "The following will be applied by this Global Theme:"
msgstr ""

#: ui/SimpleOptions.qml:32
#, kde-format
msgid "Appearance settings"
msgstr "ਦਿੱਖ ਸੈਟਿੰਗਾਂ"

#: ui/SimpleOptions.qml:39
#, kde-format
msgctxt "List item"
msgid "• Appearance settings"
msgstr "• ਦਿੱਖ ਸੈਟਿੰਗਾਂ"

#: ui/SimpleOptions.qml:45
#, fuzzy, kde-format
#| msgid "Desktop layout"
msgid "Desktop and window layout"
msgstr "ਡੈਸਕਟਾਪ ਖਾਕਾ"

#: ui/SimpleOptions.qml:52
#, fuzzy, kde-format
#| msgctxt "List item"
#| msgid "• Desktop layout"
msgctxt "List item"
msgid "• Desktop and window layout"
msgstr "• ਡੈਸਕਟਾਪ ਖਾਕਾ"

#: ui/SimpleOptions.qml:70
#, kde-format
msgid ""
"This Global Theme does not provide any applicable settings. Please contact "
"the maintainer of this Global Theme as it might be broken."
msgstr ""

#~ msgid "Get New Global Themes…"
#~ msgstr "…ਨਵੇਂ ਗਲੋਬਲ ਥੀਮ ਲਵੋ"

#~ msgid "Global Theme"
#~ msgstr "ਗਲੋਬਲ ਥੀਮ"
