msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-16 01:40+0000\n"
"PO-Revision-Date: 2021-07-25 22:54-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/ui/main.qml:70
#, kde-format
msgctxt "Opens the system settings module"
msgid "Configure Virtual Keyboards..."
msgstr ""

#: contents/ui/main.qml:89
#, kde-format
msgid "Virtual Keyboard: unavailable"
msgstr ""

#: contents/ui/main.qml:102
#, kde-format
msgid "Virtual Keyboard: disabled"
msgstr ""

#: contents/ui/main.qml:118
#, kde-format
msgid "Show Virtual Keyboard"
msgstr ""

#: contents/ui/main.qml:133
#, kde-format
msgid "Virtual Keyboard: visible"
msgstr ""

#: contents/ui/main.qml:147
#, kde-format
msgid "Virtual Keyboard: enabled"
msgstr ""
