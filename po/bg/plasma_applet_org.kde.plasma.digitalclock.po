# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2022, 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-28 01:37+0000\n"
"PO-Revision-Date: 2023-11-11 16:23+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: \n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr "Външен вид"

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr "Календар"

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:423
#, kde-format
msgid "Time Zones"
msgstr "Времеви зони"

#: package/contents/ui/CalendarView.qml:140
#, kde-format
msgid "Events"
msgstr "Събития"

#: package/contents/ui/CalendarView.qml:148
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr "Добавяне…"

#: package/contents/ui/CalendarView.qml:152
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr "Добавяне на ново събитие"

#: package/contents/ui/CalendarView.qml:384
#, kde-format
msgid "No events for today"
msgstr "Няма събития за днес"

#: package/contents/ui/CalendarView.qml:385
#, kde-format
msgid "No events for this day"
msgstr "Няма събития за този ден"

#: package/contents/ui/CalendarView.qml:433
#, kde-format
msgid "Switch…"
msgstr "Превключване…"

#: package/contents/ui/CalendarView.qml:434
#: package/contents/ui/CalendarView.qml:437
#, kde-format
msgid "Switch to another timezone"
msgstr "Превключване към друга часова зона"

#: package/contents/ui/configAppearance.qml:48
#, kde-format
msgid "Information:"
msgstr "Сведение:"

#: package/contents/ui/configAppearance.qml:52
#, kde-format
msgid "Show date"
msgstr "Показване на дата"

#: package/contents/ui/configAppearance.qml:60
#, kde-format
msgid "Adaptive location"
msgstr "Адаптивно местоположение"

#: package/contents/ui/configAppearance.qml:61
#, kde-format
msgid "Always beside time"
msgstr "Винаги отстрани на часа"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "Always below time"
msgstr "Винаги отдолу на часа"

#: package/contents/ui/configAppearance.qml:70
#, kde-format
msgid "Show seconds:"
msgstr "Показване на секунди:"

#: package/contents/ui/configAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr "Никога"

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr "Само в подсказката"

#: package/contents/ui/configAppearance.qml:74
#: package/contents/ui/configAppearance.qml:94
#, kde-format
msgid "Always"
msgstr "Винаги"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "Show time zone:"
msgstr "Показване на часова зона:"

#: package/contents/ui/configAppearance.qml:89
#, kde-format
msgid "Only when different from local time zone"
msgstr "Само ако е различно от местната часова зона"

#: package/contents/ui/configAppearance.qml:103
#, kde-format
msgid "Display time zone as:"
msgstr "Показване на часовата зона като:"

#: package/contents/ui/configAppearance.qml:108
#, kde-format
msgid "Code"
msgstr "Код"

#: package/contents/ui/configAppearance.qml:109
#, kde-format
msgid "City"
msgstr "Град"

#: package/contents/ui/configAppearance.qml:110
#, kde-format
msgid "Offset from UTC time"
msgstr "Отместване от UTC време"

#: package/contents/ui/configAppearance.qml:122
#, kde-format
msgid "Time display:"
msgstr "Показване на дата и час:"

#: package/contents/ui/configAppearance.qml:127
#, kde-format
msgid "12-Hour"
msgstr "12-часова"

#: package/contents/ui/configAppearance.qml:128
#: package/contents/ui/configCalendar.qml:51
#, kde-format
msgid "Use Region Defaults"
msgstr "Използвайте регионалните настройки по подразбиране"

#: package/contents/ui/configAppearance.qml:129
#, kde-format
msgid "24-Hour"
msgstr "24-часова"

#: package/contents/ui/configAppearance.qml:136
#, kde-format
msgid "Change Regional Settings…"
msgstr "Промяна на регионалните настройки…"

#: package/contents/ui/configAppearance.qml:147
#, kde-format
msgid "Date format:"
msgstr "Формат на датата:"

#: package/contents/ui/configAppearance.qml:155
#, kde-format
msgid "Long Date"
msgstr "Пълен формат на дата"

#: package/contents/ui/configAppearance.qml:160
#, kde-format
msgid "Short Date"
msgstr "Кратък формат на дата"

#: package/contents/ui/configAppearance.qml:165
#, kde-format
msgid "ISO Date"
msgstr "Дата на ISO"

#: package/contents/ui/configAppearance.qml:170
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr "Персонализиран"

#: package/contents/ui/configAppearance.qml:205
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"

#: package/contents/ui/configAppearance.qml:229
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr "Показване на текст:"

#: package/contents/ui/configAppearance.qml:231
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr "Автоматично"

#: package/contents/ui/configAppearance.qml:235
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr ""
"Текстът ще следва системния шрифт и ще се разширява, за да запълни наличното "
"пространство."

#: package/contents/ui/configAppearance.qml:244
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr "Ръчно"

#: package/contents/ui/configAppearance.qml:254
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr "Избор на стил…"

#: package/contents/ui/configAppearance.qml:267
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr "%1pt %2"

#: package/contents/ui/configAppearance.qml:274
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr "Избор на шрифт"

#: package/contents/ui/configCalendar.qml:38
#, kde-format
msgid "General:"
msgstr "Общи:"

#: package/contents/ui/configCalendar.qml:39
#, kde-format
msgid "Show week numbers"
msgstr "Показване на номера на седмицата"

#: package/contents/ui/configCalendar.qml:44
#, kde-format
msgid "First day of week:"
msgstr "Първи ден от седмицата:"

#: package/contents/ui/configCalendar.qml:65
#, kde-format
msgid "Available Plugins:"
msgstr "Налични приставки:"

#: package/contents/ui/configTimeZones.qml:42
#, kde-format
msgid ""
"Tip: if you travel frequently, add your home time zone to this list. It will "
"only appear when you change the systemwide time zone to something else."
msgstr ""
"Съвет: ако пътувате често, добавете още един запис за домашната си часова "
"зона към този списък. Той ще се появи само когато промените общосистемната "
"часова зона на друга."

#: package/contents/ui/configTimeZones.qml:78
#, kde-format
msgid "Clock is currently using this time zone"
msgstr "Часовникът използва понастоящем тази часова зона"

#: package/contents/ui/configTimeZones.qml:80
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the timezone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr "Скрит, докато това е местната часова зона на града"

#: package/contents/ui/configTimeZones.qml:105
#, kde-format
msgid "Switch Systemwide Time Zone…"
msgstr "Превключване на системната часова зона…"

#: package/contents/ui/configTimeZones.qml:117
#, kde-format
msgid "Remove this time zone"
msgstr "Премахване на часова зона"

#: package/contents/ui/configTimeZones.qml:127
#, kde-format
msgid "Systemwide Time Zone"
msgstr "Системна часова зона"

#: package/contents/ui/configTimeZones.qml:127
#, kde-format
msgid "Additional Time Zones"
msgstr "Допълнителни часови зони"

#: package/contents/ui/configTimeZones.qml:140
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""
"Добавете още часови зони, за да се показват всички от тях в изскачащия "
"прозорец на аплета, или използвайте една от тях за самия часовник"

#: package/contents/ui/configTimeZones.qml:149
#, kde-format
msgid "Add Time Zones…"
msgstr "Добавяне на часови зони…"

#: package/contents/ui/configTimeZones.qml:159
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr ""
"Превключване на показаната часова зона чрез превъртане над аплета на "
"часовника"

#: package/contents/ui/configTimeZones.qml:166
#, kde-format
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""
"Използването на тази функция не води до промяна на часовата зона в цялата "
"система. Когато пътувате, вместо това превключете часовата зона на системата."

#: package/contents/ui/configTimeZones.qml:190
#, kde-format
msgid "Add More Timezones"
msgstr "Добавяне на още часови зони"

#: package/contents/ui/configTimeZones.qml:201
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""
"Трябва да бъде активирана поне една часова зона. Местната ви часова зона "
"беше активирана автоматично."

#: package/contents/ui/configTimeZones.qml:236
#, kde-format
msgid "%1, %2 (%3)"
msgstr "%1, %2 (%3)"

#: package/contents/ui/configTimeZones.qml:238
#, kde-format
msgid "%1, %2"
msgstr "%1, %2"

#: package/contents/ui/main.qml:148
#, kde-format
msgid "Copy to Clipboard"
msgstr "Копиране в клипборда"

#: package/contents/ui/main.qml:152
#, kde-format
msgid "Adjust Date and Time…"
msgstr "Сверяване на дата и час…"

#: package/contents/ui/main.qml:158
#, kde-format
msgid "Set Time Format…"
msgstr "Задаване на формат на времето…"

#: package/contents/ui/Tooltip.qml:31
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr "Днес е %1"

#: package/contents/ui/Tooltip.qml:133
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr "%1:"

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr "Други календари"

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr "%1 (UNIX време)"

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr "%1 (Дата по Джулиански календар)"

#: plugin/timezonemodel.cpp:162
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr "Местно"

#: plugin/timezonemodel.cpp:164
#, kde-format
msgid "System's local time zone"
msgstr "Местна часова зона на системата"
