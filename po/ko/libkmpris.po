# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Shinjo Park <kde@peremen.name>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-15 01:54+0000\n"
"PO-Revision-Date: 2023-10-08 23:52+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.3\n"

#: multiplexermodel.cpp:71
#, kde-format
msgctxt "@action:button"
msgid "Choose player automatically"
msgstr "자동으로 재생기 선택"
